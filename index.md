---
layout: home
---

![]({{site.baseurl}}/homeindex.gif)


 <p class="text-center"> #Sharing is not scary.</p>


Here I share what I've learned and experienced during the Master in Design for Emergent Futures of 2018-19 at IAAC-Institute for Advanced Architecture of Catalonia, Barcelona.
