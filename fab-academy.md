---
layout: fab-academy
title: Fab Academy
permalink: /fab-academy/
---

# How to Make (Almost) Anything

The Fab Academy is a fast paced, hands-on learning experience where we learn rapid-prototyping by planning and executing a new project each week, resulting in a personal portfolio of technical accomplishments.
