---
title: 11. From Bits to Atoms
period: 10-16 December 2018
date: 2018-12-16 12:00:00
term: 1
published: true
---
#11. From Bits to Atoms
#### by Santi Fuentemilla, Xavi Dominguez, Eduardo Chamorro and Esteban Gimenez (Fab Lab Barcelona)


### Summary

This week we have developed a project called Scribble Sound, which is a music box that sounds differently by the color detection. The first idea was to create different sound by drawing on the surface of 3-4 lanes, however for the quality of sensors we changed to mark with tape on the surface so that it makes easier to detect the color.

### Day #1: Ideation of music machine and its prototype

Together with Julia Quiroga, Saira Raza, Silvia Ferrari, Gábor Mándoki, Oliver Juggins and Ryota Kamio, we formed a team to create a music machine. Our idea is to make a machine that makes different sounds by drawing black lines on a white surface.

![]({{site.baseurl}}/prototype.jpg)

### Day #2: Design Development

On the Day 2 we modeled the prototype in digital format, Rhinoceros and worked on the coding part for the sensors and MIDI signals in Arduino.

First, we gathered and designed the materials and parts for our Scribble Sound machine. Our neighbor, Biciclot (Carrer de Pere IV, 58, 08005 Barcelona) gave us used rubber from bicycles for the music belts and we cut them in half to fit in the size of the machine. Later We 3D printed spools so that the music belts move smooth. The fabrication took 4 hours for two pieces of the spindles.

Also we identified all the hardware and software requirments:

hardware requirements.

- Arduino Uno
- Raspberry Pi
- Contrast detecting sensors (we used two Line tracking sensor sensors & two KY-032 obstacle detect module) - both work.
- DC motor (high torque, low speed)
- Laser cut housing (file will be linked here)
- 3D printed spools (file will be linked here)
- 2 poles
- 4 bearings
- 10 spacers

software requirements:

- Arduino MIDI libary by Forty-Seven Effects (installable with the Arduino Libary manager)
- Hiduino makes the Arduino a native MIDI USB device or using a script that translates the serial input to MIDI
- Any sampler on the computer for testing (like NI Battery)
- Samplerbox Open Source sampler for Rasberry Pi


tool requirements.

- Lasercutter
- 3D printer
- Regular tools

The code for the Arduino was set up to send MIDI signals anytime the sensor sends a signal. The challenge was to figure out what note and on which channel to send.
On the first try, the Arduino send uncountable signals at the same time. So the produced signal was one big hit on the same note. To eliminate that effect, you can add a delay of 100ms. Adjust the delay as much as you want, depending on the frequency you want to send notes. The 100ms are the minimum range between each hit in this case. We tried shorter times but than the signals came to fast in our setup.
The next thing you have to take care of is to switch off the sent signal again. That's why you not only sending MIDI on signals but also MIDI off signals. Otherwise, the tone will just keep on playing as soon as it started to send the first time.

![]({{site.baseurl}}/sensorSetUp_bb.png)

### Day #3: Design process

We chnaged from the black rubber to a white fabric in the end because the rubber was to heavy for our set up and black on white delivered a better contrast for the sensor. Also we changed our original DC motor to a higher torque and lower speed model which could be powerd by a 5V power supply. We also decided not to use a pemtiometer to controll the speed as we blow up a computer during the process.

![]({{site.baseurl}}/surface2.jpg)

### Day #4: Adjusting the prototype

On day 4 we finished producing the 6 remaining wheels. The printer we used lacked the support we would have needed to create the final dividing edges of the wheels, but the belts worked anyway. Totally, it took 18 hours for us to produce 8 wheels. In retrospective, since we didn't need the edges on the wheels, we could have used CNC milling to produce them. We also created cardboard support for the four sensors.

After finishing the production, we put the modeled parts of the prototype togheter to see how they worked. The structure was too weak to tolerate the movement of the motor, wheels and belts, so we decided to improve its sturdiness by adding wood bridges and modifying the structure hosting the motor

![]({{site.baseurl}}/day4.jpeg)

### Day #5: final assembling

The final assembling went very well. We had to addjust the sensor here and there but in the end everyting fittet perfectly in the housing. We attached a battery as a power supply to the Pi and attached a speaker to the headphone jack. No the machine played drop sounds we created before whenever a black shape passed by.

![]({{site.baseurl}}/day5.jpg)

<br/>

<iframe width="560" height="315" src="https://www.youtube.com/embed/BS5BNDIuOpk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br/>

### 3D Design
#### The design of the structure for scribble sound combined a number of techniques consisting of laser cutting, 3D printing and some improvised building with scrap wood, screws, glue and a nail gun. The structure is large enough for 4 different tracks to be played at once, and its overall dimensions are approximately 300mm x 400mm. These dimensions can be altered and changed at will. Metal rods were used for the tracks to rotate around of 300mm which determined the width of the structure. The length can be changed, but note the laser cutting file will have to be altered, but this will allow for longer tracks to be used.

#### The structure is designed to be open, so the sensors and arduino are easily accessible and easy to move around when necessary. A smaller structure sits beside one of the edges in order to hold the motor at the correct height so it is inline with the metal rod for when it is rotating.

### Laser Cutting
#### The laser cutter used to cut the primary structure was a Trotec Speed 400 (1000mm x 600mm). 4mm balsa wood was used as the material and was cut with the following settings:
#### Power: 65, Speed:1, Hz: 1000.
#### These settings worked for the majority of the structure but due to the balsa wood not laying flat on the bed of the laser cutter, some elements were not cut properly and a craft knife was used to cut through the wood. The cutting less than 5 minutes and was carried out in two passes, holes and smaller elements followed by the outline. The holes were made with a tolerance of 0.15mm which worked well, but could be increased to 0.20mm as some additional sanding was necessary in parts.

### 3D Printing

#### The spindles were 3D printed. These were made to provide the tracks a larger area to rotate around and prevent them from moving around. Printers used: Hacked MakerBot for black spindles (x6) and Creality cr-10 (500mm x 500mm bed) for white spindles (x2). Files were prepared in Rhino and exported as STL. STL files were then opened and prepared for printing in Ultimaker Cura.
#### Dimensions of spindles: Diameter: 58mm, Width of dividing edge: 2.5mm, Width of spindle: 55mm, Central hole: 8.6mm
#### The metal rod used had a diameter of 8mm, and an offset of 0.3mm for the central hole is a perfect fit for the rod. This size is large enough so that it is possible to move down the rod, but small enough so there is enough friction between the rod and the inner circle of the spindle for it to rotate correctly and not spin freely when the motor is turned on. Time taken for the printing was very long ~ 18 hours for 8 spindles. To shorten this time the design could be altered so the overall diameter is smaller and the quality of the print could be reduced. The spindles were designed to be solid to provide strength, but this could also be altered so that less material is used and print time is reduced. It is also possible to stack laser-cut rings together if 3D printing is not an option and time is of the essence.

### Additional Structure
#### There were additional part of wood cut and added to the laser cut balsa wood to add stability as in initial tests with the motor the structure moved around too much. These wooden scraps were cut with standard circular saws and added with screws and a nail gun. More lateral bracing could be designed into the structure which means it could all be laser cut and have a consistent aesthetic.

<br/>

all files and guide you can find here: <https://gitlab.com/MDEF/scribble-sound>
