---
title: 10. An Introduction to Futures
period: 3-9 December 2018
date: 2018-12-09 12:00:00
term: 1
published: true
---

#10. An Introduction to Futures
#### by Elisabet Roselló (Postfuturear)

### Summary

This week we have learned the history of Futures studies from many different perspectives and also have done exercises to imagine narratives of the futures. An Introduction to Futures has quite many things to do with last week's Engaging Narratives in terms of making a scenario and narrative.


### Future(s)

When we talk about the future, we could use a lot of materials such as fiction, science fiction, art, music, etc... However, none of them tells you what exactly is going to happen in the future. In other words, it's simply impossible to presee what happens tomorrow or even 1 second after what you live right now.

So then why do we have to think about futures? I think both classic and modern futurists have studied about "directions toward futures". For instance, when we think of futures of urban mobility there are mainly two discussion: 1) More use of green energy in car industry and 2) Reduction of the use of cars. These two are often discussed in the same line, but in terms of directions toward the futures of urban mobility it's quite different. One intends to maintain the use of cars and the other does the opossite, therefore you see that totally opossite directions are discussed in the same level. Apparently the both 1) and 2) could interact with each other in the process toward each direction, even though the objectives of each scenario differ.    

At the same time, it is important to see the paces of changes on different layers of our society. When you look at the graphic of Layers of Change you see how often technology changes and how the technology has potential to share futures depending on its use.


###### ![]({{site.baseurl}}/Layers of change.png) Source: Grandcraft (http://www.grantcraft.org/takeaways/layers-of-change)


### Speculative design as an emergent field of Futures Studies

One of the themes that interested me most during the week of An Introduction to Futures has been Speculative design. Actually the speculative design is a continuity of the last week's lecture and since then it's been one of the main insprirations when it comes to thinking of my own final project.

Major isses we have today are in the last layers such as culture and nature, but at the same time the elemnts that affect most the last layers are often the outer layers such as technology and bussiness models. Therefore it's hard to change but also it's easy to change if we want to use technology and business models in right way. Speculative design helps us to advance our society with how we want your future to look like tomorrow rather than what you can do realistically.

My area of interest: Living is such a wicked problem too, since it's in a massive complex of economy, politic and urban design. However, I believe that the more complucated the problems are the more we may need to speculate our ideas for common good.     

![]({{site.baseurl}}/IMG_2985.jpg)

![]({{site.baseurl}}/IMG_3018.JPG)

![]({{site.baseurl}}/Futures of Living.png)
