---
title: 07. The Way Things Work
period: 12-18 November 2018
date: 2018-11-18 12:00:00
term: 1
published: true
---

### by Guillem Camprodon, Victor Barberan and Oscar Gonzalez (Smart Citizen)



### Summary
This week's lectures have explored The Way Things Work from existing electronics to Arduino and coding. Mainly we've revealed how simple the electronics are made and how similar each parts of the different electronics are. Besides that, we've re-learned the basic of Arduino which we also learned during the pre-course period and played with the input and output via WiFi.


### Shall we get our hands-on?
First thing we did on the first day of the class was introducing ourselves to physical computing by hacking everyday products such as printers, phone, electric water boiler, etc… It was simply fun to see what's inside our everyday electronics and how electronics companies make benefits out of their products, for instance the ink cartridge in case of printers.

![]({{site.baseurl}}/W7_photo1.jpeg)    

 # Arduino

After refreshing the memory about the basic of Arduino, we, as a team of 6 people: Adriana Tamargo Iturri, Julia Danae Bertolaso, Maite Villar Latasa, Laura Álvarez Florez, Vesa Gashi and me, started to work on group project about How to Sensor Happiness. We intended to measure the happiness by pitch of the voice or moves, however it turned out to be too complicated to make it in such limited times. Therefore, we shifted to work on the output with LEDs and servomotors instead.

In particular, we tried to make the output of sensed data visualised in color by LEDs and moves by servomotors. It provides several different palettes of colors by the input data. Via WiFi we sent the input data to our Arduino and then the LED strip moves with different color patterns.    

![]({{site.baseurl}}/W7_graphic1.gif)    


### How is this relevant to your work?
It has been a week of literally Learning by Doing. I have been exploring what I'm capable to do with Arduino and other equipments, which has helped me understand a little bit of how electronic production is designed and the potential of Arduino.

I personally have participated at DECODE project in which I'm supposed to sense via Smart Citizen Kit 2.0. I'm excited to learn the Arduino made sensor as an user and hopefully it will help me understand the mechanism more profoundly.     
