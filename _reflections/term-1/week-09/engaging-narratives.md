---
title: 09. Engaging Narratives
period: 26 November - 2 December 2018
date: 2018-12-02 12:00:00
term: 1
published: true
---

#09. Engaging Narratives
#### by Heather Corcoran (Kickstarter), Bjarke Calvin (Duckling), Mariana Quintero and Tomas Diez

### Summary
This week's lectures have dealt with narratives and storytellings of yor project. We have talked different platforms to spread your ideas from crowdfunding platforms or TED to an App. Also, through the storytelling exercises we presented the story/narrative of our own projects at the last day of the week.   

### Lessons from Heather Corcoran (Kickstater)

Heather Corcoran is the European Design & Technology Outreach at Kickstarter.

She explained us the activities of Kickstarter as a potential platform of crowdfunding to bring creative projects to life. Througn her experience as the Outreach Lead of Kickstater she taught us how to tell/present our project. In particular, she lectured us that we should use simple and clear design/format and clear and descriptive langugae to describe your project. Specifically Title (60 characters), Subtitle/Tag (130 characters) and Sketch a Lead Image.

We also read "A creative person’s guide to thoughtful promotion" by Kathryn Jaller as a reference of practical strategies to promote your creative projects, especially on SNS and emails.

### Lessons from Bjarke Calvin (Duckling.co)

Bjarke Calvin is the CEO of Duckling.co and a fellow at MIT Open Doclab.

He explained us major transitions from social media to insight media to create collective wisdom. Based on his own App, Duckling he taught us storytelling techniques: Give people a reason to care, Build your idea and Make it worth sharing.

![]({{site.baseurl}}/Duckling_Ryota.gif)

I find the Duckling very interesting and useful to practise storytelling skill and also I use it as a reflection tool to digest a big amount of information I have received.

### Reflection

![]({{site.baseurl}}/Sharing-is-not-scary.gif)

At the end of the week we presented a narrative of our own final projects.

To put my ideas in order I mapped my area of interest in different scales according to Multiscalar design strategy graphic by Tomas Diez. It helped me realize what I'm interested most and identify my interest in each scale. I realized that my interest is not housing but exploring new ways of living in city environment.

Also after talking to Tomas and Mariana I started to think the combination of action research and playful and specurative design might be applicable in my interest, living. I could start to research current housing issues in Barcelona as a repository and then exolore what could specurate and play.       
