---
title: 03. Design for the Real Digital World
period: 15-21 October 2018
date: 2018-10-21 12:00:00
term: 1
published: true
---
#03. Design for the Real Digital World
##### by Ingi Freyr Guðjónsson and Francesco Zonca
-

##### ![]({{site.baseurl}}/DSC_0484.jpeg) (Photo credit: Ryota Kamio)


## Reflection
#### What did you do that is new. What did you learn?

> Practically everything was new to me.

The whole process of making a product in a group was an new experience to me. First of all, I must admit that I was the only one in the group thinking of making a furniture when we got the task, while the others was designing the whole room...   

![]({{site.baseurl}}/WhatsApp Image 2018-10-16 at 13.02.30.jpeg)

> Team: Maite Villar, Tom Barnes, Ola Łukaszewska, Rutvij Pathak, Ryota Kamio, Vicky Simitopoulou, Vesa Gashi

Our proposal was to create makers space, since we foresee that we may need spaciois spaces for our hands-on works through the comming Fab academy. The resason why we came to this decision is that we felt the need of high stand tables, shelfs for our tools and most importantly no human traffic jam when it comes to "making".

#### The process we've followed is:

01. Picking up trashes from the streets of Poble-nou neighborhood.
02. Brainstorming on what we need for our classroom / we can make out of the trashes.
03. Classification of our brainstormed ideas into several groups of similarities.
04. Identification of items to make = moduable table and stable table.
05. Collection of inspiration of moduable/stable tables for the makers space.
06. Collection/identification of materials (trashes from Poble-nou and Fab Lab Barcelona) for our makers space.
07. 3D modeling of the design of the makers space.
08. Brainstorming on additional items/design for the makers space such as wall hanger, LED lights, plugs, etc...
09. Use of CNC machine and Laser cutter.
10. Assembling the component parts with glue or screws.


> The whole process is explained here: <https://issuu.com/olalukaszewska/docs/design_for_the_real_world__day1_2f2>

#### Leaning from the team working on product design

Identification of the products to make is the first challenge in a team work, in which normally you see in how different ways we think of the same product like a table. Also often the identification process differs between people with product design experience and people with conceptual design experience or none. Therefore, I've learned that identifying the work flow and the roles for each team member is very much needed when it comes to designing in such a limited time. At the same time exploring ideas and reframing the design or concept from different points of view of each member was definitely appreciatable.

###### ![]({{site.baseurl}}/DSC_0531.jpeg)(Photo credit: Ryota Kamio)

In terms of skills we've basically utilized 2 new skills: CNC machine and Laser cutter. Before utilizing them you must measure the thickness and length of the materials carefully, otherwise the whole plan goes wrong and multiplies work. Also after the use of them it facilitates me to use Rhino because it helps me capture the image of what I can do with Rhino in reality.
The use of those 2 skills still requires a lot of practice but this week has helped me improve my design skills.


## How can your learning process be described?
- Fieldwork (collecting trashes)
- Touching materials
- Brainstorming
- Co-design
- Co-development
- Inspiration
- Sketching
- Clustering
- 3D modelling by Rhinoceros

## How is this relevant to your work?
This week has helped me develop design skill and express what I think on my mind in a physical thing, which is basically one of my goals during the MDEF. Moreover, there have been a lot of learning from working in a group of 6 people to design or create a solution for a space, since in particular co-design has been one of the key words for me to learn.

For exemple, one of the learning through our Makers space project is that co-design or horizontal design or participatory design requires a strong framework to let every single participant express him/herself and most importantly gather everyone's ideas to one objective. Visualization helps a lot to share the same image or view in order for everyone to work towards the same direction. Personally since the week of Design for the Real Digital World I've been trying to take notes in different way than before, in which I always keep in mind how to deliver my message in more efficient way to other people. In other words, taking notes not only for my input but also for expressing my understanding to other people with graphics or photos, which makes a lot of differences when it comes to input.      
