---
title: 04. Exploring Hybrid Profiles in Design
period: 22-28 October 2018
date: 2018-10-28 12:00:00
term: 1
published: true
---
#04. Exploring Hybrid Profiles in Design
##### by Ariel Guersenzvaig (Elisava), Sara De Ubieta, Pol Trias and Marta Handenawer (Domestic Data Streamers) and Oscar Tomico

-
<iframe src="https://player.vimeo.com/video/297891707" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/297891707">Exploring Hybrid Profiles in Design</a> from <a href="https://vimeo.com/user91177114">Ryota Kamio</a> on <a href="https://vimeo.com">Vimeo</a>.</p>


## Reflection
#### #1 Ariel Guersenzvaig (ELISAVA - Director of the MUDIC and Co-director of the Master in Design and Internet Web Project Direction)

(Short) timeline of Ariel Guersenzvaig's career

> 1. Information scientist
2. Information architect
3. User experience designer
4. Service designer


##### "NOW" = Design researcher on ethical digital transformation

Ariel is an inspiring design researcher who has researched the design from multidisciplinary approaches such as ethic, ethnography and psychology.

First of all, Ariel's definition or understanding of design opened a lot of possibilities to discuss what is design and what design brings you. Also his research on the relationship between design and decision making brought us inspiring insights not only to product or systemic design but to design for uncertainty or emergent futures. The most interesting point for me was that he has approached design from other disciplines, which largely was a significant shift from engineering discipline to social-science discipline (information science to philosophy in his case).

There have been a big trend of introduction of anthropology or ethnography in information science, design and business disciplines. I see that it's because the engineering approach which generally captures things or condition by number has felt the need of search of what is behind the numbers. For instance, from Big-data to Thick-data as Ariel said. I found it interesting his view because design plays an important role to make this shift smooth. I see that a practitioner and thinker like Ariel is more needed to dive in what design is and why we design.

-

#### #2 Sara González de Ubieta

(Short) timeline of Sara González de Ubieta's career

> 1. Graduated from BSc of Architecture at UPC
2. Architect at a few architect studios based in Barcelona  
3. European debt crisis hit and her work places were shut down around 2010
4. Carrer change to a shoe maker

##### "NOW" = Shoe maker and material researcher


She is an interesting hybrid profile who has changed her career from architect to a shoe crafter and material researcher.  

She said "when I was working as an architect I couldn't see the outcome of what I was doing". Her attitude of looking for the transparency in her own work definitely reflects her actual work and it enriches her research of materials. She criticised that many designers, especially in shoe industry, don't do research enough therefore they don't know the potential of materials. She made me realise that the habit of research in design field such as fashion industry is very much required today, since we're facing environmental and social issues for massive consumption of our everyday goods.     

###### ![]({{site.baseurl}}/IMG_2683.JPG) (Photo credit: Ryota Kamio)

-
#### #2 Domestic Data Streamer

(Short) timeline of Domestic Data Streamer's career

> 1. Graduated from Elisava
> 2. Designer at IoT department of Telefónica
> 3. Researcher in Japanese sub-culture

##### "NOW" = Product designer to data enabled communication consultant

Pol and Marta are very motivated data-driven storytellers with unique backgrounds and a great variety of profiles at their office.

Their wide range of such innovative research and design projects were very interesting to listen and sounded all very exciting. Especially their effort to maintain their projects playful and diverse was the essential skill of them. The long-term vision of investment on meaningful projects and well-organized frameworks of "how to progress project" managed through APP and most importantly, visualization of the data are their strong tools.

I also realized that data visualization is such a powerful and practical tool to empower many social issues as they do. Human-oriented approach to the data is more required and emergent technology helps a lot to boost the quality.

-

## Who Am I? How do I want to be?
![]({{site.baseurl}}/Dream.jpg)

- I have obtained a lot of knowledge from my past studies in urban studies, but don't have enough skill to express it.
- I tend to think in "too" big scale to design something, therefore I'd like to start in small scale and scale it up to bigger scale step by step.
- I should spend the same amount of time in research and design, since so far I've spent much time in research and not much in the design process.
- I'd like to keep the balance of the both social-scientific and engineering approaches in my projects to be playful, human-oriented and efficient.
- What's my design? - I'd like to find a piece of my own design theory during the MDEF.

## Personal Development Plan

###### ![]({{site.baseurl}}/Thinker.jpg) (Photo credit: Ryota Kamio)

So far, in the MDEF lectures I've seen many different scales of "design" from product design (Design for the Real Digital World) to design theories by hybrid profiles and thinkers.

I like to learn how different designers produce the outcomes from such wide ranges of design activities. Probably for such a limited time to develop my final project I may have to decide what design tool to use and what design process to use as the references during the first semester. At the same time, I like to start to plan my project for the following 10 years which should be a big scale design such as concept or systemic design, therefore developing my final project of the MDEF will be the first stage of this big plan.

> I may like to have some time when we can see the outcomes of the projects by the designers who have lectured us in a workshop or by interviewing the users. It's important for me to know how non-designers perceive their projects. For instance, I suppose that there may be a lot of differences between product design and participatory or systemic design from points of view of the users.   

## My vision (of the future) as a designer

![]({{site.baseurl}}/Color.jpg)

I see myself to become a designer that will always provoke to be aware of social isses around us and enlighten to act on the issues in collaboration with more players in the society. Therefore, storytelling skill must be the key.  

Moreover, I will always try to be a transdisciplinary designer, which promises that I don't always put myself in my comfort zone but try to challenge different disciplines.   

-
-
Guersenzvaig, Ariel (2013) Design rationality revisited: describing and explaining design decision making from a naturalistic outlook. University of Southampton, School of Management, Doctoral Thesis, 197pp. (<https://eprints.soton.ac.uk/354613/1/Final%2520PhD%2520thesis%2520-%2520Ariel%2520Guersenzvaig.pdf>)
