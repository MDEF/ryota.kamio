---
title: Emergent Business Models
period: 16 May - 30 May 2019
date: 2019-05-16 12:00:00
term: 3
published: true
---
![]({{site.baseurl}}/Slides images.png)

![]({{site.baseurl}}/Ryota Kamio_Submission session 2 - Newspaper of the future_MDEF.jpg)

### Sustainable model for my project

When it comes to thinking sustainable business model for an innovation, how to maximize impact, minimize efforts and maximize probability on systemic scale should be well considered as indicators. The sustainability in current and emerging business model is equivalent to creating an ecosystem, which includes people, space, funding, knowledge, value creation and knowledge.

Co-living is a new born business sector and has been growing first for the last 5 years. But still it lacks most of the elements in order to create the ecosystem. For instance, a co-living agency, Conscious Co-living defines in their Co-living 3.0 model the following challenges for a sustainable co-living:

1. Community
2. Affordabilit
3. Scalability
4. Individuality
5. Adaptability
6. Government Cooperation
7. Ownership

One of the challenges that co-living generally faces is 2. Affordability and how to sustain the affordability in the co-living sector is still on the experimental phase. Recently SPACE10, EFFEKT Architects and Norgram Studio presented a vision of the affordability of housings which tackles the interests of short-term investors and challenge existing models of development. For example, a standardized modular building system that would be pre-fabricated, mass-produced and flat-packed and democratic setups inspired by community land trusts and co-operatives. However, business sustainability in co-housing sector still has a lot of spaces to explore and innovate. 
