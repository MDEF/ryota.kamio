---
title: Design For the New
period: 26 April - 17 May 2019
date: 2019-04-26 12:00:00
term: 3
published: true
---

### Bitacora describing my learning process

During the lectures of Design for the New we reflected our projects in performs of everyday life = "everydayness". In my case, new models for co-living.

For me projecting our futuristic project in everydayness means that sometimes you're being mean with your own idea in order to see how your idea gets installed to the reality, because sometimes it doesn't go well like you expect.

On the other hand, in the process of installing our intervention idea in the everydayness gives you insights which often are looked over in design for futures.

#### 1. Current practice analysis

![]({{site.baseurl}}/HOLON ex1.png)

By analysing current everydayness I could identify a certain patterns when people are gathered and work collectively. For instance, food is often in the center of the gatherness in a housing and around the food there are always collective activities such as buying/growing ingredients, cooking, preparing the dishes/table and eating. It became a great insight for my project, which made me think of collective activities rather than the space itself.

Another example is setting a certain house rule in order to maintain the ownership over our own space. It's our everyday behaviour to have the sense of community or belonging to the space. Even though it is said that the millennials are not interested at ownership but experience (which is true), in term of sharing a physical space like a home the ownership still matters.

#### 2. Desired practice analysis

![]({{site.baseurl}}/HOLON ex2.png)

Based on the current practice analysis I tried to project a practise in the futures, but it took me while to do so because my intervention was not very clear or a tangible design.

However, I tried to imagine a desired scenario first of my co-living idea and stuff and image around it. Basically, in my desired co-living there will be more shared spaces and shared responsibility over our own living spaces which means more integration and also energy sufficiency.

In order to grow collectiveness one of the practices that I identified for his exercise is _Making/Designing_ our own space, in particular living space. Through the current practise analysis I identified eating or sharing a meal as a collective everyday practise, therefore I tried to find similar protocols. And it was Makers movement. In today's society entertainment such as watching TV is embedded in our phone, therefore there is no need to get gathered to watch TV with our house members. However, the new (digital) technological revolution has made it possible to share our knowledge and produce physical objects out of the shared knowledge. I think _Making_ can be replaced from watching TV or entertainment, traditional activities in living room.

#### 3. Intervention transition pathway

![]({{site.baseurl}}/HOLON ex3.png)

This exercise is based on the both current practice analysis and desired practice analysis.

Overall, the learning process has helped me to narrow down the idea and identify possible interventions. I honestly wish we could have done all those exercises earlier in the 1st or 2nd trimester, so that it would accelerate the intervention identification.  

### Intervention prototype

![]({{site.baseurl}}/LEGO.jpg)


As I proposed in the Desired practice analysis, my intervention prototype is _Making/Designing_ our own living room to grow collectiveness through the process of making. I'm preparing the prototype for the exposition of our final project in the coming weeks.

In the process of social change and technological revolution, traditional activities such as gathering in living room to watch TV together and mingle have gradually dissapeared. For instance, in Hong Kong or Tokyo already living room has replaced to a private room to receive more people in a housing. Today, the majority of activities that were expected to practice when living room emerged in the late 19th or early 20th century has been embedded in our personal digital devices, while the space design of housing hasn’t changed much for such changes. Therefore, the living room needs new activities corresponding to new social demands to be used as a common space. And my ideas is that it can be a space to express the knowledge generated on the open source platforms such as digital fabrication. TV or video game used to attract people to use the living room, and what’s left there now? During the research there was an interesting fact that often kitchen is used as a common spaces more than living rooom, and I realized that sharing food is still people’s favorite activity. Around sharing a mean there are many activities such as buying/growing ingredients, cooking, preparing dishes, eating, cleaning the dishes, etc... And food is something which always has had innovations, because we experience it everyday. Designing our own living room can apply the same protocols as cooking. From choosing the materials until using it with other people.

What I suggest here is to bring recycle and upcycle mentality to design for the living room by using waste collected from the streets as the principal materials. The idea was inspired by MADE AGAIN project led by Fab Lab Barcelona and IKEA’s Future Living Lab, SPACE10 as a prototype of locally productive and globally connected Fab City in Poble-nou district, Barcelona. Especially, by means of digital fabrication, it explores the possibility of a new design by the potential of materials rather than industrial materials.

Today, such interactive digital media has created open source platforms where share the knowledge about (almost) everything. For instance, digital fabrication has enabled us to design from bits to atoms, which means the shared knowledge can be produced as a tangible object in the real world.       about (almost) everything. For instance, digital fabrication has enabled us to design from bits to atoms, which means the shared knowledge can be produced as a tangible object in the real world.      

I present the living room made by waste during the exposition on 24th of June, 2019 - September, 2019.  
