---
title: Designing for 2050
period: 25 April - 03 May 2019
date: 2019-04-25 12:00:00
term: 3
published: true
---

The lectures of Designing for 2050 by Andres Colmenares (<a href="https://www.internetagemedia.com/">Internet Age Media</a>) have been playful and interactive. And importantly, we used "futures (plural)" as tools to design for futures.  

## Critical optimism - Planetary approach - Long-term strategy

### Exercise

Towards the end of the lectures we made an alternative "Black mirror" episode for 2050, combining each of our projects' topics in. The theme of the whole Season is "PRISMATIC MINDS" which is decided collectively by all of us in the class.

Jess, Silvia, Fífa, Oliver and I decided to work on an episode "Good Kenko (good health in Japanese language), in which describes the extreme aging society of Tokyo in 2050.  

![]({{site.baseurl}}/kenko2.png)

1) _Title_: Good Kenko
2) _City_: Tokyo
3) _Main character_: Taeko (former fashion designer)
4) _Context_: Tokyo in 2050 is facing an extreme aging society, where the health system doesn't follow the demands of the citizens and is collapsed. With the introduction of AI sensored network a number of collective communities of health care were born to engage in self-awareness and environmental affects. Thanks to the technological advance it has succeed to reduce sickness in the city, but still social loneliness or disconnection matter.   

Enjoy!

<iframe src="https://player.vimeo.com/video/341238982" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/341238982">Good Kenko</a> from <a href="https://vimeo.com/user91336952">F&iacute;fa J&oacute;nsd&oacute;ttir</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### Learning

During the creation of the video we were being careful of not being too critical or dark with a touch of optimism. This effort let us narrow down to the point which can work with emerging technologies in health care industry and should critically be concerned.

At the same time, what I found interesting is that the whole of series of lectures were playful and joyful (with a lot of fun GIFs and videos). When it comes to discussing about the "weak signals" we have identified during the master degrees, often the discussion gets too serious or pessimistic. However, during the courses of Designing for 2050 Critical optimism + Playful made us a blank space to let what doesn't make sense happen, which sometimes creates an innovation. Randomness.


<iframe src="https://giphy.com/embed/Xfq4B3p1KwepW" width="480" height="360" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/design-innovation-Xfq4B3p1KwepW">via GIPHY</a></p>


Also, identifying tools helped us identify issues around the topics easily and tells the audience a whole story in indirect way.    


## #Internet citizens #Futures in plural #Futures as tools #Futures to be invented #Internet of beings #Shared reality #Collaborative learning #Human experience
