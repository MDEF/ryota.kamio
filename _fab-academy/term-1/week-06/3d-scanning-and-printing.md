---
title: 06. 3D scanning and printing
period: 27 February 2019
date: 2019-02-27 12:00:00
term: 1
published: true
---
### Week 6 assignment was to design and 3D print an object, and additionally 3D scan the object.

For this week's assignment I decided to 3D scan and print myself.
There are many open source design online, which is the interesting part of 3D but this time I decided to 3D print something that no one else can do it without me, myself.


1) Set the XBOX 360 camera and <a href="https://skanect.occipital.com/ ">Skanect</a> (this time I used the computer at Fab Lab).

2) 3D scan myself with another person's support slowly and patiently and export it in STL.

3) Clean the image on Rhinoceros and set a box under the 3D scanned "me" to stabilize.

4) 3D print it.

The required equipments and materials:

- XBOX 360 camera
- Skanect
- Rhinoceros
- play
- ANYCUBIC 3D printer
- PLA

### 3D scanning

<iframe src="https://player.vimeo.com/video/322850703" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/322850703">3D scanning</a> from <a href="https://vimeo.com/user91177114">Ryota Kamio</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

After setting the camera at the right spot I started to scan and rotate myself and at the end I scanned from the top. While scanning you have to rotate yourself or chair (if you sit in movable chair) VERY SLOWLY so that the scanner can capture you as much as possible.

### Editing the scanned image on Rhinoceros

![]({{site.baseurl}}/3D Rhino_Ryota.png)

The original scanned file has no stability in order to 3D, so that I added a stand on the bottom. Also I cleaned some black dots generated during the scanning.

### 3D printing

![]({{site.baseurl}}/3D support_Ryota.JPG)

![]({{site.baseurl}}/3D_Ryota.jpg)

Once the file is ready, it's time to print it. Since some parts of the figure were not stable, I added some supporters.

### Learning

Being critical 3D printing still takes so much time to make an "easy" object. However, it makes possible to design and print a 3 dimensional object in such easy steps. This time, for instance, a scanned image into an object.

At the same time, subtractive method enables to reduce the use of material and importantly when you see that something goes wrong you can stop the machine and see where it starts to go wrong. In terms of design 3D printer enables to control more depth, length and shapes more than other technologies such as laser cutter or CNC machine. It's because basically the size or shape of the material doesn't limit your design like materials used for the machines mentioned above. Moreover, 3D printing is a great technique for rapid prototyping that enables quick design changes and allows designers to modify their design before the serial production.  

Here you can download the [**file**]({{site.baseurl}}/Ryota5.3dm)
