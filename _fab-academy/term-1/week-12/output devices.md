---
title: 12. Output Devices
period: 10 April 2019
date: 2019-04-10 12:00:00
term: 1
published: true
---
### Week 12 assignment was to output something: replicate a Neil's board.

Alex and I decided to work together on input and output assignments in the same board. The board is designed to use for our final exposition, which makes the air pump work only for 1min per hour by using a transistor in order to control the current of the battery to the air pump. ATtiny45 has a PWM pin that will work for our board.

First of all, we checked the air pump with the power supply, to see the voltage and current needed= 3.5V for 0.16A. We used a 3.7V Li-ion battery.

![]({{site.baseurl}}/battery.jpg) ![]({{site.baseurl}}/air pump.jpg)

<iframe src="https://player.vimeo.com/video/340193271" width="640" height="352" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/340193271">voltage</a> from <a href="https://vimeo.com/user91177114">Ryota Kamio</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### Designing our own board

It's the same design as the board for <a class="text_black" href="https://mdef.gitlab.io/ryota.kamio/fab-academy/input-devices/"> Week11  W11. Input devices</a> </u> assignment.

For the output we designed the board with a N mosfet transistor connected to the digital output pin of the ATtiny45 to control the timing when the air pump opens and closes.

![]({{site.baseurl}}/our board2.png)

### Programming our own board

To save time and understand better, first we used a library called _tinysnore_ that shuts down the ATtiny45 board in Arduino environment.

```
#include
#include
#include "tinysnore.h" // Include TinySnore Library

int aPpin = 1; //airPump

void setup()
{
pinMode(aPpin, OUTPUT); //AIR PUMP PIN
digitalWrite(aPpin, LOW);
}

void loop()
{
digitalWrite(aPpin, HIGH); // turn on air pump
delay(60000); //1 min air pump on
digitalWrite(aPpin, LOW); //turn of
snore(3540000); // Deep sleeps for 59 minutes, low power consumption
}

```
<iframe src="https://player.vimeo.com/video/340193480" width="640" height="1138" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/340193480">test</a> from <a href="https://vimeo.com/user91177114">Ryota Kamio</a> on <a href="https://vimeo.com">Vimeo</a>.</p>


We programmed the board with the codes mentioned above following the same steps as <a class="text_black" href="https://mdef.gitlab.io/ryota.kamio/fab-academy/embedded-programming/"> Week9 Embedded programming</a>.

This is how it worked!

<iframe src="https://player.vimeo.com/video/340193735" width="640" height="1138" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/340193735">final result</a> from <a href="https://vimeo.com/user91177114">Ryota Kamio</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### Problems that we fixed

During the exercise we didn't have much problems thanks to a lot of practices we have done during Fab Academy. But using the library _tinysnore_ was very helpful and saves us a lot of time.

Here you can download the [**file and pictures**]({{site.baseurl}}/W12_RyotaKamio.zip).
