---
title: 05. Electronics production
period: 20 February 2019
date: 2019-02-20 12:00:00
term: 1
published: true
---
### Week 5 assignment was make an in-circuit programmer by milling the PCB, program it, then optionally try other PCB processes.

The process of creating your own PCS is following:

1) Getting the board file at 1000 DPI resolution on PNG format and then preparing it through Fab Modules.

2) Milling it on CNC machine

3) Soldering the components

4) Programming the ISP

The required parts are:

- 1 ATTiny 44 microcontroller
- 1 Capacitor 1uF
- 2 Capacitor 10 pF
- 2 Resistor 100 ohm
- 1 Resistor 499 ohm
- 1 Resistor 1K ohm
- 1 Resistor 10K
- one 6 pin header
- 1 USB connector
- 2 jumpers - 0 ohm resistors
- 1 Crystal 20MHz
- two Zener Diode 3.3 V
- one usb mini cable
- one ribbon cable
- two 6 pin connectors

This time we all used FabISP, which is an in-system programmer for AVR microcontrollers, designed for production within a FabLab.
You can review more information on <a href="http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week4_electronic_production/fabisp.html">Fab ISP</a> and <a href="http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html">Building the FabTinyISP</a>.

### Milling (SRM-20 CNC)

Once you prepare your files on PNG format open them on Fab Modules in order to make it into a .rmv file. I downloaded the traces and outline from <a href="http://academy.cba.mit.edu/classes/electronics_production/index.html">Fab Academy</a> page.

Import the file into FabModules the png file and select Roland as the output. And then select the select 1/64 (thin traces) to mill the traces and select 1/32 (thick traces) to mill the outline. Importantly make sure that Standard speed is 4 and machine origin is 0,0,0. Once everything above is set you push Calculate, which basically calculates all the other things automatically.

![]({{site.baseurl}}/fabmodule.png)

Choose the machine to mill your PCB board (this time I chose SRM-20). he board must be clean and importantly must stick to the base with double-sided tape so that it doesn't move in the middle of milling. Set the tip on 0 point with allen key and once it's set on the point you unfold the tip again till it touches the board. When everything is set, click "Cut" to select the files and "Output" button to start the milling.  

![]({{site.baseurl}}/FA_W5_4.jpg)


### Soldering

Soldering is a long process to end for beginners and needs certain techniques like crafting. Techniques that I have learned are 1) set the board in a stable place 2) use tweezers 3) place the solder iron on where you want to solder a bit longer than you think enough 4) put relaxing music.

![]({{site.baseurl}}/FA_W5_1.JPG)

![]({{site.baseurl}}/FA_W5_3.JPG)

### Programming

During a smoke test I found an issue around USB connector. Burned...

I tried to fix it, however the board was not recognized by my laptop... Therefore, I will revenge it for the comming Week 7 of Electronics Design, in which you design your own PCB board again from the scratch.
