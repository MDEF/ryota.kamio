---
title: 17. Wildcard week - Welding
period: 08 March 2019
date: 2019-05-08 12:00:00
term: 1
published: true
---
### Week 17 assignment was to try and document one of the three wildcard week proposed processes for this week.


Welding is a fabrication or sculptural process that joins materials, usually metals or thermoplastics, by using high heat to melt the parts together and allowing them to cool causing fusion. Welding is distinct from lower temperature metal-joining techniques such as brazing and soldering, which do not melt the base metal.
In short, putting two metal pieces into one.

Welding machine works with electricity through filament. The machine has a welding gun from which comes filament and electricity to weld. The electricity is connected to positive and negative charges provided by the ground cable and attached to the metal surface.

### Safety first

What you need for welding:
1. Jacket
2. Welding helmet
3. leather groves (no rubber)

You should be careful with the cable to the welding gun not be bent, as it has the filament inside. Once you think you're ready, you ask your buddy "ready?" to confirm if everyone around you is ready for the welding.

When you weld it's recommended to use a digital helmet which protects your eyes from sparkling lights during welding (analog one is fine, but the sight is all black).

When the material is too thin it makes a hole easily in the process of welding, therefore it's recommended to use a metal with at least more than 3mm thickness.

### Practice - Welding

You set the welding gun on the metal plate with 45 degrees, so that you can see the line between metal plates. When you weld well, you see a red circle and it follows the line as you move your gun. You need a little patience to always keep the red circle in the line.

![]({{site.baseurl}}/Welding.gif)

Also when you weld well, it sounds differently (not sparky sound).

![]({{site.baseurl}}/P1020637.jpg)

### Practice - Grinding

![]({{site.baseurl}}/Grinding.gif)

Once you welded two metal into an once piece, you should "clean" it by grinding. The process is same as other sanding process, but as always be mindful with the safety protocols.

![]({{site.baseurl}}/IMG_4553.JPG)
