---
title: 03. Computer-aided design
period: 06 February 2019
date: 2019-02-06 12:00:00
term: 1
published: true
---
### Week 3 assignment was prototyping your (possible) final project in raster, vector, 2D, 3D, render, animate, simulate, etc...

As I'm totally new to 3D modelling and parametric design, firstly I reviewed and tried some of the computer-aided design (CAD) softwares such as Rhinoceros (+ Grasshopper), Fusion 360 and OpenCAD. After some trials with those softwares I decided to use Fusion 360 for this week's assignment, since I consider it slightly more user-friendly than other softwares and it's also easier to find information online.

As I mentioned above, I'm a total beginner of the CAD design and still can't picture my final project therefore first of all I followed <a href="https://youtu.be/aloVo93i65s">a Youtube tutorial</a> that includes certain skills that I want to develop during the Fab Academy.

The image of the goal is following:

![]({{site.baseurl}}/W3_pic1.png)


# 1) Create Sketch & Line

On Fusion 360 the first thing to do is you have to select on _SKETCH_ command and _Create Sketch_ to start a project. And always remember to select the unit in _mm_ from Document Settings.

And then you select _Line_ command or just type _L_ to draw lines. So this time I drew 3 lines (40mm x 140mm x 25mm). And from the top of the line you draw arch with _3-Point Arc_ command from _CREATE_ until the bottom.

![]({{site.baseurl}}/W3_pic2.png)

![]({{site.baseurl}}/W3_pic3.png)

![]({{site.baseurl}}/W3_pic4.png)

![]({{site.baseurl}}/W3_pic5.png)

![]({{site.baseurl}}/W3_pic6.png)

# 2) Revolve

Once you get all the lines and arches set, you select _Revolve_ from _CREATE_ and set 360 degrees.

![]({{site.baseurl}}/W3_pic7.png)

# 3) Shell

From _MODIFY_ you select _Shell_ command in order to make hole. You select the top and bottom and you set 20mm on Inside Thickness.

![]({{site.baseurl}}/W3_pic8.png)

![]({{site.baseurl}}/W3_pic9.png)

![]({{site.baseurl}}/W3_pic10.png)

# 4) Slicer for Fusion 360

Slicer for Fusion 360 is an extension of Fusion 360 and "it is a tool to turn your digital 3D models into appealing artefacts. It slices and converts 3D models into 2D patterns that you can cut out of any flat material. Slicer for Fusion 360 also creates 3D instructions you can interact with, to help build a model (Autodesk, Inc)".

So, firstly you have to download it and once you get it downloaded you see Slicer for Fusion 360 on _MAKE_. And you select the object that you want to slice.

![]({{site.baseurl}}/W3_pic11.png)

![]({{site.baseurl}}/W3_pic12.png)

Once it's imported you select types of slice from _Construction Technique_. This time I selected _Radial Slices_ and modified 1st Axis (7) and Radial (5) numbers on _Slice Distribution_.  

![]({{site.baseurl}}/W3_pic13.png)

Here you can download the [**file**]({{site.baseurl}}/Revolve_slicer v1 v3.f3d) and [**1D file**]({{site.baseurl}}/SFlasercut.dxf).
