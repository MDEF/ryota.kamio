---
title: 04. Computer-controlled cutting
period: 13 February 2019
date: 2019-02-13 12:00:00
term: 1
published: true
---
### Week 4 assignment was design, lasercut, and document a parametric design.

I used the same design (sliced lamp) as the last week's assignment of Computer-aided design, since it already has the press fit construction.

Firstly, I didn't realize that the design I developed last week is not correct because the many parts are marked red (everything has to be in grey). Therefore, I started to trouble shoot the red parts of the former design. You can see the image of the red parts in the lamp I made on <a href="https://mdef.gitlab.io/ryota.kamio/fab-academy/computer-aided-design/">W3 Computer-aided design</a>.

<br/>

<iframe src="https://player.vimeo.com/video/317064064" width="640" height="386" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/317064064">Slicer lamp</a> from <a href="https://vimeo.com/user91177114">Ryota Kamio</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

<br/>


I moved some parts and modified the thickness and offset on Slicer (Fusion 360), however there was no way to have it all in grey. So I exported the file to Rhinoceros in order to try to fin it in the material that I liked to use, cardboard.

![]({{site.baseurl}}/press kit.png)

I decided to test small pieces of the lamp in order to see if the measurements are fine. I set the cut outs in the rectangle in 4.1 mm, which is 0.1 mm wider than the width of the cardboard I picked.

However it was so loose that it modified it to 4.0mm at the end.

![]({{site.baseurl}}/lasercut process.jpeg)


Once the file is ready, it's time to laser cut.
I adjust the power, speed, and hertz parameters by the standard values suggested. I set my cardboard in the laser cutter and tape it so that it doesn't move while cutting. The next step is to set the lens hight with a stick that is always in the pocket on the right side of the machine. Also make sure that the fume extractor is On.

When it's all set, it's time to print!

![]({{site.baseurl}}/FA_W4_1.jpg)

![]({{site.baseurl}}/FA_W4_2.jpg)

![]({{site.baseurl}}/FA_W4_3.jpg)


Here you can download the [**file**]({{site.baseurl}}/lamp2-0.3dm)


### Vinyl cutter

When I made the lamp, I totally forgot to do Vinyl cutter assignment so I took advantage of this for my final project.

I made chroma screen sticker for my futuristic phone that is going to be used in my design fiction movie.

![]({{site.baseurl}}/vinyl1.jpeg)

I prepared the file in the illustrator to cover the "screen" and exported it in oldest illustrator file (this time I used illustrator 3 format) so that the printer Roland GX-24 Vinyl Cutter recognize the file. (I've seen other classmates have used dxf file, but I used ai. file).

Once you set your vinyl, set the adjustment with the width and insert it till the bottom. And push ``Enter`` on the right side of the printer for the machine to measure the size of the vinyl, so that you can only click ``Get from Machine``.

When it's all set, it's time to print!

 ![]({{site.baseurl}}/IMG_4494.JPG)
 (I changed the color of the vinyl for the chroma)


Here you can download the [**file**]({{site.baseurl}}/v_cutter_phone.AI) and [**file**]({{site.baseurl}}/laser_cut_phones.3dm).
