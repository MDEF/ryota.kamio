---
title: 09. Embedded Programming
period: 20 March 2019
date: 2019-03-20 12:00:00
term: 1
published: true
---
### Week 9 assignment was to program your board to do something, with as many different programming languages and programming environments as possible.

### Reading a microcontroller datasheet

The board we programmed contains an ATtiny45 which is a microcontroller. Before reading ATiny45 datasheet it’s important to understand the difference between microcontroller and microprocessor.
<u> <a class="text_black" href="https://www.farnell.com/datasheets/1698186.pdf"> Download ATtiny45 datasheet</a> </u>

- A microcontroller or MCU is a simpler computer, which contains a CPU (central processing unit), a fixed amount of RAM (Random Access Memory), ROM (Read-Only Memory) and other peripherals all embedded onto a single chip. In short, it works like a small computer with everything embedded in it.
- A microprocessor is an integrated circuit which typically contains the CPU. In short, needs to be connected with RAM and ROM to work.  

In order to program the board mainly C language (Computer Language) is used, and Arduino IDE is the same mechanism but a lot more simplified than the C language. This pin layout explains the roles of each pin.

![]({{site.baseurl}}/pin configulations.png)
![]({{site.baseurl}}/pinout.jpg)

### Programming the board

Here Alex and I decided to combine the assignments of W10. Input devise and W11. Output devise to practice to program the board.

Our circuit is based on a ATtiny45, to program this board we followed this steps:

 1. In Arduino go to "archivo" / preferences / add url for attiny boards: https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json

![]({{site.baseurl}}/Screen1.png)


 2. Go to Tools / Boards / Boards manager and download attiny library:

![]({{site.baseurl}}/Screen2.png)

 3. Later we set up the features to program an attiny45 as you can see in the image below:

`Board: ATtiny25/45/85.
Procesador: ATtiny45
Clock: internal 1MHz (because we didn't add any external)
Programmer: USBtinyISP because we used FabISP`

![]({{site.baseurl}}/Screen3.png)

4. Programming the board

```
//libraries
 #include
 #include
 #include "tinysnore.h" // Include TinySnore Library

//variables
int pTpin = 3; //photoTransistor
int aPpin = 1; //airPump

int pTvalue = 0; //
int pTthreshold = 900;

//setup
void setup()
{  
pinMode(aPpin, OUTPUT); //AIR PUMP PIN
digitalWrite(aPpin, LOW);
pinMode(pTpin, INPUT); //PHOTORESISTOR INPUT PIN
}
//loop
void loop()
{
pTvalue = analogRead(pTpin);
if (pTvalue >= pTthreshold)
{ digitalWrite(aPpin, HIGH); }
else { digitalWrite(aPpin, LOW); }
snore(5000); // Deep sleeps for 5 seconds, (low power) then resumes from here
}

```

![]({{site.baseurl}}/done.jpeg)
