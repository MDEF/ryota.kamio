---
title: 11. Input Devices
period: 03 April 2019
date: 2019-04-03 12:00:00
term: 1
published: true
---
### Week 11 assignment was to measure something: add a sensor to a microcontroller board that you have designed and read it.



Alex and I decided to work together on input and output assignments in the same board. The board is designed to use for our final exposition, which turns on/off air pump to a mini hydroponic. We took a LDR sensor (=photoresitor) in order to detect light intensity, so that the air pump turns off during the night time alone.

### Designing our own board

With little time we had, we took the Niel's <a class="text_black" href="http://academy.cba.mit.edu/classes/output_devices/speaker/hello.speaker.45.png"> hello.speaker.45</a> as a reference. However, in the end, we had to add 3 jumpers which don't exist in the Niel's board in order to add the battery and air pump. Therefore, the design looks totally different than the Niel's one in the end.

![]({{site.baseurl}}/design.png)

Once we identified the components, they should be connected to corresponding spots.

![]({{site.baseurl}}/niel speaker.png) Niel's hello.speaker.45.py
![]({{site.baseurl}}/our board.png) Our board

Once it's designed, you mill the board following the same steps as <a class="text_black" href="https://mdef.gitlab.io/ryota.kamio/fab-academy/electronics-production////"> Week5 Electronic production</a>.

![]({{site.baseurl}}/kkk.gif)


### Programming our own board

We looked up online to see how LDR sensor works to read an analog input, and found the code and Arduino circuit. We checked different thresholds to find the perfect one that turn our circuit off when it is dark =nignt time.

![]({{site.baseurl}}/Arduino1.png) ![]({{site.baseurl}}/Arduino2.png)

```
const int LEDPin = 13;
const int LDRPin = A0;
const int threshold = 100;

void setup() {
pinMode(LEDPin, OUTPUT);
pinMode(LDRPin, INPUT);
}

void loop() {
int input = analogRead(LDRPin);
if (input > threshold) {
digitalWrite(LEDPin, HIGH);
}
else {
digitalWrite(LEDPin, LOW);
}
```

<iframe src="https://player.vimeo.com/video/340191878" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/340191878">Arduino 3</a> from <a href="https://vimeo.com/user91177114">Ryota Kamio</a> on <a href="https://vimeo.com">Vimeo</a>.</p>


[]({{site.baseurl}}/Arduino4.gif)

### Problems that we fixed

- Firstly we put a phototransistor on our board, however it didn't work. So we unsoldered it and added a LDR (= photoresistor).
- We changed threshold number several times, and finally it turned out that _100_ is the most reliable number to respond to the light intensity.  

Here you can download the [**file and pictures**]({{site.baseurl}}/W10_RyotaKamio.zip).

<u><a class="text_black" href="http://academy.cba.mit.edu/classes/input_devices/index.html" target="_blank">FabAcademy input devices</a></u>

<u><a class="text_black" href="http://fab.academany.org/2019/labs/barcelona/local/wa/week10/" target="_blank">FabBarcelona input devices</a></u>
